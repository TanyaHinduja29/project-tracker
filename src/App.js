import { useEffect, useState } from 'react';
import './App.css';
import Header from './components/Header';
import { ProjectCard } from './components/ProjectCard';
import { Container, Row } from 'react-bootstrap';
import { v4 as uuid } from "uuid";
import { EditProjectModal } from './components/EditProjectModal';

function App() {
  const description = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nesciunt repellendus enim doloremque nisi similique dolorum laboriosam culpa quisquam nihil id, cupiditate quasi aliquam deserunt molestiae, quis recusandae eos veritatis? Error!";
  // const projects = [
  //   {name: "Project 1", price: "45", description, status: "Ongoing"},
  //   {name: "Project 2", price: "65", description, status: "Ongoing"},
  //   {name: "Project 2", price: "65", description, status: "Completed"}
  // ];

  

  const [modalShow, setModalShow] = useState(false);
  const [editProject, setEditProject] = useState(null);

  const [projects, setProjects] = useState(
    JSON.parse(localStorage.getItem("projects")) || []
  );

  useEffect(() => {
    localStorage.setItem("projects", JSON.stringify(projects));
  }, [projects]);


  function handleModalClose() {
    setModalShow(false);
    setEditProject(null);
  }
  function handleEdit(projectId){
    setModalShow(true);

    for (let i = 0; i < projects.length; i++) {
      if (projects[i].id === projectId) {
        setEditProject(projects[i]);
        break;
      }
    }
  }
  function handleAddProject(project){
    const newProject = {
      id: uuid(),
      name: project.name,
      description: project.description,
      price: project.price,
      status: project.status ,
      elapsed: 0,
      runningSince: null
    };
    setProjects([...projects, newProject]);
  }
  function handleUpdateProject(updatedProject, projectId){
    setProjects(
      projects.map((project) => {
        if (project.id === projectId) {
          return {
            /*We did this because here it might change ellapse timings */
            ...project,
            name: updatedProject.name,
            description: updatedProject.description,
            price: updatedProject.price,
            status: updatedProject.status,
          };
        }
        return project;
      })
    );

  }
  function handleStartTimer(projectId){
    setProjects(
      projects.map((project) => {
        if(project.id === projectId){
          return {
            ...project, runningSince: Date.now()
          };
        }
        return project;
      })
    );
  }

  function handleStopTimer(projectId){
    setProjects(
      projects.map((project) =>{
        if(project.id === projectId){
          const totalElapsed = project.elapsed + (Date.now() - project.runningSince);
          return {...project, runningSince:null, elapsed: totalElapsed};
        }
        return project;
      })
    )
  }
  function handleDelete(projectId) {
    setProjects(projects.filter((project) => project.id !== projectId));
  }


  return (
    <>
      <Header onSubmit={handleAddProject}/>
      <Container className="mt-3">
        <Row>
         {projects.map((project) => (
          <ProjectCard 
          key={project.id} 
          project = {project} 
          onTimerStart={handleStartTimer}
          onTimerStop={handleStopTimer}
          onEdit={handleEdit}
          onDelete={handleDelete}
          />
         ))}
        </Row>
        <EditProjectModal
          show={modalShow}
          onClose={handleModalClose}
          project={editProject}
          onSubmit={handleUpdateProject}
        />
      </Container>
    </>
  );
}

export default App;