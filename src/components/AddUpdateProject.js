import React, { useEffect, useRef } from 'react'
import { Button, Form } from 'react-bootstrap'

const AddUpdateProject = ({onSubmit, onHide, project}) => {
  const projectNameRef = useRef();
  const projectDescriptionRef = useRef();
  const projectPriceRef = useRef();
  const projectStatusRef = useRef();

  useEffect(() => {
    projectNameRef.current.value = project?.name || "";
    projectDescriptionRef.current.value = project?.description || "";
    projectPriceRef.current.value = project?.price || "";
    projectStatusRef.current.value = project?.status || "ongoing";
  }, []);


  function resetFields(){
    projectNameRef.current.value = "";
    projectDescriptionRef.current.value="";
    projectPriceRef.current.value= "";
    projectStatusRef.current.value = "ongoing";
  }
  function handleAddProject(evt){
    evt.preventDefault();
    const name = projectNameRef.current.value;
    const description = projectDescriptionRef.current.value;
    const  price = projectPriceRef.current.value;
    const status = projectStatusRef.current.value;
    onSubmit({name, description, price, status});
    resetFields();
    onHide();
  }
  function handleUpdateProject(evt) {
    evt.preventDefault();
    const name = projectNameRef.current.value;
    const description = projectDescriptionRef.current.value;
    const price = projectPriceRef.current.value;
    const status = projectStatusRef.current.value;
    onSubmit({ name, description, price, status }, project.id);
    resetFields();
    onHide();
  }


  return (
    <Form onSubmit={project ? handleUpdateProject : handleAddProject}>
        <Form.Group className="mb-3">
          <Form.Label>Project Name</Form.Label>
          <Form.Control
              type="text"
              placeholder="Enter project name"
              ref = {projectNameRef}
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Project Description</Form.Label>
          <Form.Control
              type="text"
              placeholder="Enter project description"
              ref = {projectDescriptionRef}
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Price Per Hour</Form.Label>
          <Form.Control
              type="text"
              placeholder="Enter price per hour"
              ref = {projectPriceRef}
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Select Status</Form.Label>
          <Form.Select aria-label="Default select example" ref={projectStatusRef}>
            <option value="ongoing">Ongoing</option>
            <option value="completed">Completed</option>
          </Form.Select>
        </Form.Group>
        <Button type="submit" variant="primary">
          {project ? "Update Project" : "Add Project"}
        </Button>
    </Form>
  )
}

export default AddUpdateProject