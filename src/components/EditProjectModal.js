import React from 'react'
import { Modal } from 'react-bootstrap'
import AddUpdateProject from './AddUpdateProject'

export const EditProjectModal = ({show, onClose, project, onSubmit}) => {
    if(!project){
        return <></>
    }
  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton>
        <Modal.Title>Update Project</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <AddUpdateProject
          project={project}
          onHide={onClose}
          onSubmit={onSubmit}
        />
        
      </Modal.Body>
    </Modal>

  )
}
