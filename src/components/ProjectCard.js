import { faPencilSquare, faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useEffect, useState } from 'react'
import { Badge, Button, Card, Col, Stack } from 'react-bootstrap'
import { StartStopTimerButton } from './StartStopTimerButton'
import Helpers from '../Helpers'

export const ProjectCard = ({project, onTimerStart, onTimerStop, onEdit, onDelete}) => {
    function handleStartTimer(){
        onTimerStart(project.id);
    }
    function handleStopTimer(){
        onTimerStop(project.id);
    }
    const [updateCount, setUpdateCount] = useState(0);

    useEffect(() =>{
        const interval = setInterval(() => setUpdateCount(updateCount + 1), 100);
        //cleanup
        return () => clearInterval(interval);
    }, [updateCount]);
  return (
    <Col md={3}>
            <Card border="info">
              <Card.Header>
                <Stack direction="horizontal">
                  <h4 className='me-auto'>{project.name}</h4>
                  <div className="vr" />
                  <p className='my-auto ms-2'>${project.price}</p>
                </Stack>
              </Card.Header>
              <Card.Body>
                <Card.Text>
                  <span className='d-block'>{project.description}</span>
                  <h2 className='text-center'>
                    {Helpers.renderElapsedString(
                    project.elapsed,
                    project.runningSince
                    )}
                  </h2>
                </Card.Text>

                <Stack direction="horizontal" gap={3} className='mt-4'>
                  <Badge pill 
                  bg={project.status === "completed"? "success" : "info"} 
                  size="md">{project.status}</Badge>
                  <Button 
                  variant="outline-danger" 
                  className='ms-auto' 
                  size="sm"
                  disabled={project.status === "completed"}
                  onClick={() => onDelete(project.id)}
                  ><FontAwesomeIcon icon={faTrash}/></Button>

                  <Button 
                  variant="outline-warning" 
                  size="sm"
                  disabled={project.status === "completed"}
                  onClick={() => onEdit(project.id)}
                  >
                <FontAwesomeIcon icon={faPencilSquare}/></Button>
                </Stack>
                <div className="d-grid mt-2">
                  <StartStopTimerButton
                  status={project.status}
                  runningSince={project.runningSince}
                  onTimerStart={handleStartTimer}
                  onTimerStop={handleStopTimer}
                  >

                  </StartStopTimerButton>
                </div>
              </Card.Body>
            </Card>
    </Col>
  )
}
