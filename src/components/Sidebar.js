import React from 'react'
import { Offcanvas } from 'react-bootstrap'
import AddUpdateProject from './AddUpdateProject'

const Sidebar = ({show, handleClose, onSubmit}) => {
  return (
    <Offcanvas show={show} onHide={handleClose} placement='end'>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Add Project</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <AddUpdateProject onSubmit={onSubmit} onHide={handleClose} />
        </Offcanvas.Body>
    </Offcanvas>
  )
}

export default Sidebar